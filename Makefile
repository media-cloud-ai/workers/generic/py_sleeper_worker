DOCKER_REGISTRY?=registry.gitlab.com
DOCKER_IMG_NAME?=media-cloud-ai/workers/generic/py_sleeper_worker
SOURCE_DIR=*.py
TEST_DIR=./tests
ifneq ($(DOCKER_REGISTRY), )
	DOCKER_IMG_NAME := /${DOCKER_IMG_NAME}
endif

VERSION?=`cat VERSION`

docker = docker run -v ${PWD}:/sources -v ${DATA_FOLDER}:/sources/data --rm ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION}

docker-build:
	@docker build -t ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} -f Dockerfile .

docker-clean:
	@docker rmi ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION}

docker-registry-login:
	@docker login --username "${CI_REGISTRY_USER}" -p ${CI_REGISTRY_PASSWORD} ${DOCKER_REGISTRY}

docker-push-registry:
	@docker push ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION}

code-coverage:
	@coverage run --source=${SOURCE_DIR} -m unittest discover
	@coverage report -m

code-format:
	@autopep8 --in-place --aggressive --recursive ${SOURCE_DIR}
	# @autopep8 --in-place --aggressive --recursive ${TEST_DIR}

code-lint:
	@pylint --output-format=text ${SOURCE_DIR} | tee ./pylint/pylint.log

check-version:
	@$(eval code := $(shell export DOCKER_CLI_EXPERIMENTAL=enabled; \
		docker manifest inspect ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} > /dev/null \
		&& echo 0 || echo 1))
	@if [ "${code}" = "0" ]; then \
		echo "image ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} already exists."; exit 1;\
	else \
		echo "image ${DOCKER_REGISTRY}${DOCKER_IMG_NAME}:${VERSION} is available."; \
	fi
