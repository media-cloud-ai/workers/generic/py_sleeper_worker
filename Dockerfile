FROM registry.gitlab.com/media-cloud-ai/sdks/py_mcai_worker_sdk:1.1.3

WORKDIR /sources

ADD . /sources

RUN apt-get update --allow-releaseinfo-change && \
    apt-get install -y python3-pip && \
    pip3 install -r requirements.txt

ENV PYTHON_WORKER_FILENAME=/sources/worker.py
ENV AMQP_QUEUE=job_sleeper

CMD py_mcai_worker_sdk
