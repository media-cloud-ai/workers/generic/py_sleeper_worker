import logging
import os
import socket
import sys
import time


def get_name():
    return "py_sleeper_worker"


def get_short_description():
    return "python worker that only sleeps"


def get_description():
    return """py_sleeper_worker worker is a python worker that only sleeps
    for a given amount of seconds, logging its progression.
    """


def get_version():
    return "0.0.1"


def get_parameters():
    return [
        {
            "identifier": "source_path",
            "kind": ["string"],
            "label": "Path of the reference",
        },
        {
            "identifier": "sleep",
            "kind": ["integer"],
            "label": "Number of seconds the worker should sleep",
        },
        {
            "identifier": "destination_path",
            "kind": ["string"],
            "label": "Destination path of the offset json result",
        },
    ]


def init():
    return


def process(handle_callback, parameters, job_id):
    log_level = os.environ.get("RUST_LOG", "warning").upper()
    logging.basicConfig(
        stream=sys.stdout,
        level=getattr(logging, log_level),
        format="%(asctime)s.%(msecs)03d000000 UTC - {container_id:s} - {job_queue:s} - {jobid:d} - %(levelname)s - %(message)s".format(
            container_id=socket.gethostname(),
            job_queue=os.getenv("AMQP_QUEUE", default="unknown_queue"),
            jobid=job_id,
        ),
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    sleeping_time = parameters.get("sleep", 25)

    for i in range(0, sleeping_time):
        if handle_callback.is_stopped():
            # Set job status to stop and return
            handle_callback.set_job_status("stopped")
            return {"destination_paths": ["/path/to/generated/file.ext"]}

        time.sleep(1)

        # notify the progression (between 0 and 100)
        handle_callback.publish_job_progression(i * 100 // sleeping_time)

    return
